/*******************************************************************************
 * Product Routes
 ******************************************************************************/
 'use strict';

 const ProductController = require('../controllers/ProductsController'),
   auth = require('../helpers/auth').userValidate,
   validator = require('express-joi-validator'),
   ProductSchema = require('../validations/product-schema');
 
 module.exports = (app) => {
   /**
     * @swagger
     *  /api/products:
     *    get:
     *      summary: product list by user
     *      tags: [Product]
     *      security:
     *        - bearerAuth: []
     *      parameters:
     *        - in: header
     *          name: accept-language
     *          schema:
     *              type: string
     *              enum: [en, it]
     *          required: true
     *        - in: query
     *          name: name
     *          description: Product's name/title for search
     *          schema:
     *              type: string
     *        - in: query
     *          name: page
     *          description: Page number for pagination
     *          schema:
     *              type: number
     *        - in: query
     *          name: limit
     *          description: Limit for pagination
     *          schema:
     *              type: number
     *      responses:
     *        "200":
     *          description: Success
     *          content:
     *            application/json:
     *              schema:
     *                $ref: '#/components/schemas/List'
     */
   app.get('/api/products', validator(ProductSchema.list), auth, (req, res) => {
     ProductController.productList(req, res);
   });
 
   /**
    * @swagger
    * paths:
    *  /api/product/{id}:
    *    get:
    *      summary: Product view by user.
    *      tags: [Product]
    *      security:
    *        - bearerAuth: []
    *      parameters:
    *        - in: header
    *          name: accept-language
    *          schema:
    *              type: string
    *              enum: [en, it]
    *          required: true
    *        - in: path
    *          name: id
    *          schema:
    *              type: string
    *          required: true
    *      responses:
    *        "200":
    *          description: Success
    */
   app.get('/api/product/:id', auth, (req, res) => {
     ProductController.view(req, res);
   });
 
   /**
    * @swagger
    *  /api/product:
    *    post:
    *      summary: Add Product by User
    *      tags: [Product]
    *      security:
    *        - bearerAuth: []
    *      parameters:
    *        - in: header
    *          name: accept-language
    *          schema:
    *              type: string
    *              enum: [en, it]
    *          required: true
    *      requestBody:
    *        required: true
    *        content:
    *          application/json:
    *            schema:
    *              $ref: '#/components/schemas/AddProduct'
    *      responses:
    *        "200":
    *          description: Success
    *          content:
    *            application/json:
    *              schema:
    *                $ref: '#/components/schemas/AddProduct'
    */
    app.post('/api/product', validator(ProductSchema.addProduct), auth, (req, res) => {
     ProductController.addProduct(req, res);
   });

   /**
   * @swagger
   *  /api/product/{id}:
   *    put:
   *      summary: Update Product by User
   *      tags: [Product]
   *      security:
   *        - bearerAuth: []
   *      parameters:
   *        - in: header
   *          name: accept-language
   *          schema:
   *              type: string
   *              enum: [en, it]
   *          required: true
   *        - in: path
   *          name: id
   *          schema:
   *              type: string
   *          required: true
   *      requestBody:
   *        required: true
   *        content:
   *          application/json:
   *            schema:
   *              $ref: '#/components/schemas/ProductUpdate'
   *      responses:
   *        "200":
   *          description: Success
   *          content:
   *            application/json:
   *              schema:
   *                $ref: '#/components/schemas/ProductUpdate'
   */
  app.put('/api/product/:id', validator(ProductSchema.update), auth, (req, res) => {
    ProductController.update(req, res);
  });

  /**
    * @swagger
    * paths:
    *  /api/product/{id}:
    *    delete:
    *      summary: Removed product's details by user.
    *      tags: [Product]
    *      security:
    *        - bearerAuth: []
    *      parameters:
    *        - in: header
    *          name: accept-language
    *          schema:
    *              type: string
    *              enum: [en, it]
    *          required: true
    *        - in: path
    *          name: id
    *          schema:
    *              type: string
    *          required: true
    *      responses:
    *        "200":
    *          description: Success
    */
   app.delete('/api/product/:id', auth, (req, res) => {
    ProductController.remove(req, res);
  });
 };