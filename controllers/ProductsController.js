const mongoose = require('mongoose'),
  Product = mongoose.model('Product'),
  Q = require('q'),
  CommonHelper = require('../helpers/common'),
  Logger = require('../helpers/logger'),
  ProductHelper = require('../helpers/product'),
  productTransformer = require('../transformers/product');

const addProduct = async (req, res) => {
  try {
    let createdBy = req.auth.credentials._id;
    req.body.createdBy = createdBy;
    req.body.slug = CommonHelper.getSlugData(req.body.title)
    const product = Product(req.body);
    await product.save();
    res.status(200).send({
      success: true,
      message: res.__('productAdded'),
      data: { _id: product._id },
    });
  } catch (err) {
    Logger.add('Error in add product by user', err);
    return res.boom.badRequest(err);
  }
};

const productList = async (req, res) => {
  try {
    let offset = req.query.page ? (req.query.page - 1) * req.query.limit : 0;
    let limit = req.query.limit ? parseInt(req.query.limit) : 10;
    let query = { createdBy: CommonHelper.stringToObjectId(req.auth.credentials._id) };
    //search by titleEN/titleIT
    if (req.query.name) {
      if (!query.$and) {
        query.$and = [];
      }
      const name = CommonHelper.searchUserData(req.query.name);
      query.$and.push({ $or: [{ title: name }, { slug: name }] });
    }
    let products = await Q.all([
      Product.countDocuments(query).exec(),
      Product.aggregate([{ $match: query }, { $sort: { created_at: -1 } }]).allowDiskUse(true).skip(parseInt(offset)).limit(parseInt(limit))
    ]);
    const result = {
      success: true,
      message: res.__('productListing'),
      data: productTransformer.productList(products[1]),
      totalCount: products[0]
    };
    return res.status(200).send(result);
  } catch (err) {
    Logger.add('Product Listing Error ', err);
    return res.boom.badRequest(err);
  }
};

const view = async (req, res) => {
  try {
    let { id } = req.params;
    id = CommonHelper.getObjectId(id);
    if (!id) return res.boom.notFound(res.__('invalidProductId'));
    let query = { _id: id, createdBy: CommonHelper.stringToObjectId(req.auth.credentials._id) };
    let product = await Product.aggregate([{ $match: query } ]).allowDiskUse(true);
    if (!product[0]) return res.boom.notFound(res.__('invalidProductId'));
    const result = {
      success: true,
      message: res.__('productDetails'),
      data: await productTransformer.productDetails(product[0]),
    };
    return res.status(200).send(result);
  } catch (err) {
    Logger.add('Product View Error ', err);
    return res.boom.badRequest(err);
  }
};

const update = async (req, res) => {
  try {
    const { id } = req.params;
    const productId = ProductHelper.getProductId(id);
    if (!productId) return res.boom.notFound(res.__('invalidProductId'));
    let { title, shortDescription, description, price, quantity } = req.body;
    const product = await Product.findOne({_id: productId, createdBy: CommonHelper.stringToObjectId(req.auth.credentials._id) });
    if (!product) return res.boom.notFound(res.__('invalidProductId'));
    product.title = title;
    product.slug = CommonHelper.getSlugData(title);
    product.shortDescription = shortDescription;
    product.description = description;
    product.price = price;
    product.quantity = quantity;
    await product.save();
    res.status(200).send({
      success: true,
      message: res.__('productUpdate'),
      data: { _id: product._id },
    });
  } catch (err) {
    Logger.add('Product Update Error ', err);
    return res.boom.badRequest(err);
  }
};

const remove = async (req, res) => {
  try {
    let { id } = req.params;
    id = CommonHelper.getObjectId(id);
    if (!id) return res.boom.notFound(res.__('invalidProductId'));
    const product = await Q.all([ Product.findOne({_id: id, createdBy: CommonHelper.stringToObjectId(req.auth.credentials._id) }) ]);
    if (!product[0]) return res.boom.notFound(res.__('invalidProductId'));
    await product[0].remove();
    res.status(200).send({
      success: true,
      message: res.__('productDeleted'),
      data: null,
    });
  } catch (err) {
    Logger.add('Product Delete Error', err);
    return res.boom.badRequest(err);
  }
};


module.exports = {
  productList,
  view,
  addProduct,
  update,
  remove
};
