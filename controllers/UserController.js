/*******************************************************************************
 * User Controller
 ******************************************************************************/
 'use strict';
 const mongoose = require('mongoose'),
   User = mongoose.model('User'),
   Product = mongoose.model('Product'),
   Logger = require('../helpers/logger'),
   UserHelper = require('../helpers/user'),
   faker = require("faker"),
   Q = require('q'),
   CommonHelper = require('../helpers/common'),
   UserTransformer = require('../transformers/user');
 
 const signup = async (req, res) => {
   try {
     const { email } = req.body;
     const isValid = CommonHelper.isEmailValid(email)
     if (!isValid) return res.boom.conflict(res.__('InvalidEmail'));
     const userInfo = await UserHelper.emailExist(email);
     console.log(userInfo, 'userInfo++++')
     if (userInfo) return res.boom.conflict(res.__('EmailTaken'));
     const userObject = UserHelper.getUserObject(req.body, req.headers['accept-language']);
     const user = new User(userObject);
     user.emailVerifyToken = UserHelper.getAuthTokens(user);
     const userData = await user.save();
     // Send signup mail
     await UserHelper.sendSignupMail(userData);
     res.status(200).send({
       success: true,
       message: res.__('SignupSuccess'),
       data: null,
     });
   } catch (err) {
     return res.boom.badRequest(err);
   }
 };
 
 const userLogin = async (req, res) => {
   try {
     const { email, password } = req.body;
     const user = await UserHelper.emailExist(email);
     if (!user) return res.boom.notFound(res.__('InvalidLogin'));
     const isMatchPassword = UserHelper.comparePassword(password, user.password);
     if (!isMatchPassword) return res.boom.unauthorized(res.__('InvalidLogin'));
     const userInfo = await user.save();
     res.status(200).send({
       success: true,
       token: UserHelper.getAuthTokens(userInfo),
       message: res.__('LoginSuccess'),
       data: UserTransformer.userloginDetails(userInfo),
     });
   } catch (err) {
     return res.boom.badRequest(err);
   }
 };
 
 const getUserProfile = async (req, res) => {
   try {
     const { _id } = req.auth.credentials;
     const user = await User.findById(_id);
     if (!user) return res.boom.notFound(res.__('InvalidUserId'));
     res.status(200).send({
       success: true,
       message: res.__('UserProfile'),
       data: UserTransformer.userProfile(user),
     });
   } catch (err) {
     Logger.add('Profile Detail Error ', err);
     return res.boom.badRequest(err);
   }
 };

 const getUsersList = async (req, res) => {
  try {
    let offset = req.query.page ? (req.query.page - 1) * req.query.limit : 0;
    let limit = parseInt(req.query.limit ? req.query.limit : 10);
    let query = { };
    let users = await Q.all([User.countDocuments(query).exec(), User.find(query).sort({ created_at: -1 }).skip(parseInt(offset)).limit(parseInt(limit))]);
    const result = {
      success: true,
      message: res.__('UserListing'),
      data: UserTransformer.userList(users[1]),
      totalCount: users[0],
    };
    return res.status(200).send(result);
  } catch (err) {
    Logger.add('Users Listing Error ', err);
    return res.boom.badRequest(err);
  }
};
 
 module.exports = {
   signup,
   userLogin,
   getUserProfile,
   getUsersList
 };
 