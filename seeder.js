const seeder = require('mongoose-seed');
const faker = require("faker");
const CommonHelper = require('./helpers/common');
const Config = require('./config/config');
const bcrypt = require('bcrypt');
const db = 'mongodb://127.0.0.1:27017/testapp-db';

const hashPassword = (password) => {
    password = CommonHelper.removeEmptySpace(password);
    return bcrypt.hashSync(password, Config.saltRounds);
};
// Connect to MongoDB via Mongoose
seeder.connect(db, () => {
    // Load Mongoose models
    seeder.loadModels(['./models/User.js']);
    // Clear specified collections
    seeder.clearModels(['User'], () => {
        let userData = [];
        for(let i=0; i<10; i++) {
            userData.push({
                email: CommonHelper.toLowerCase(CommonHelper.removeEmptySpace(faker.internet.email(faker.name.firstName(), faker.name.lastName()))),
                firstName: faker.name.firstName(),
                lastName: faker.name.lastName(),
                fullName: `${faker.name.firstName()} ${faker.name.lastName()}`,
                password: hashPassword('qwerty')
            });
        }
        let data = [{
            'model': 'User',
            'documents': userData
        }]
        seeder.populateModels(data, (err, done) => {
            if(err) console.log('Seed err', err);
            if(done) console.log("Seed done", done);
            seeder.disconnect();
        });
    });
});