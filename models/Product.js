/*******************************************************************************
 * Product Model
 ******************************************************************************/
 'use strict';
 const mongoose = require('mongoose'),
   Schema = mongoose.Schema,
   timestamps = require('mongoose-timestamps');
 
 
 //Define Product schema
 const ProductSchema = new Schema({
   createdBy: { type: Schema.Types.ObjectId, ref: 'User', required: true },
   title: { type: String, default: '' },
   slug: { type: String, default: '' },
   shortDescription: { type: String, default: '' },
   description: { type: String, default: '' },
   price: { type: Number, default: 0 },
   quantity: { type: Number, default: 0 }
 });
 
 //middle ware in serial
 ProductSchema.pre('save', (next) => {
   this.updated_at = Date.now();
   next();
 });
 
 // Add timestamp plugin
 ProductSchema.plugin(timestamps, { index: true });
 module.exports = mongoose.model('Product', ProductSchema);
 