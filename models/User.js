/*******************************************************************************
 * User Model
 ******************************************************************************/
 'use strict';
 const mongoose = require('mongoose'),
   Schema = mongoose.Schema,
   timestamps = require('mongoose-timestamps');
 
 
 //Define User schema
 const UserSchema = new Schema({
   fullName: { type: String, default: '' },
   firstName: { type: String, default: '' },
   lastName: { type: String, default: '' },
   email: { type: String, default: '' },
   password: { type: String, default: '' }
 });
 
 // Add timestamp plugin
 UserSchema.plugin(timestamps, { index: true });
 
 module.exports = mongoose.model('User', UserSchema);
 