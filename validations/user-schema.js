const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);

/**
 * @swagger
 *  components:
 *    schemas:
 *      UserSignup:
 *        type: object
 *        required:
 *          - firstName
 *          - lastName
 *          - email
 *          - password
 *        properties:
 *          firstName:
 *            type: string
 *            format: firstName
 *            description: FirstName of the user.
 *          lastName:
 *            type: string
 *            format: lastName
 *            description: lastname of the user
 *          email:
 *            type: string
 *            format: email
 *            description: Email of the user.
 *          password:
 *            type: string
 *            description: Password of the user.
 */
module.exports.signup = {
  body: {
    firstName: Joi.string().required(),
    lastName: Joi.string().required(),
    email: Joi.string().required().email(),
    password: Joi.string().required()
  },
};

/**
 * @swagger
 *  components:
 *    schemas:
 *      UserProfile:
 *        type: object
 */

/**
 * @swagger
 *  components:
 *    schemas:
 *      UserLogin:
 *        type: object
 *        required:
 *          - email
 *          - password
 *        properties:
 *          email:
 *            type: string
 *            format: email
 *            description: Email for the admin, which was used in signup.
 *          password:
 *            type: string
 *            description: Password for your account that you choosed in signup.
 */
module.exports.Userlogin = {
  body: {
    email: Joi.string().required(),
    password: Joi.string().required()
  },
};
