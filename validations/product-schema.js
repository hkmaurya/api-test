const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);

/**
 * @swagger
 *  components:
 *    schemas:
 *      List:
 *        type: object
 *        properties:
 *          name:
 *            type: string
 *            description: product's name/title for search
 *          page:
 *             type: number
 *          limit:
 *              type: number
 */
module.exports.list = {
  query: {
    name: Joi.string().optional().allow(''),
    page: Joi.number(),
    limit: Joi.number(),
  }
};

/**
 * @swagger
 *  components:
 *    schemas:
 *      AddProduct:
 *        type: object
 *        required:
 *          - title
 *          - shortDescription
 *          - description
 *          - price
 *          - quantity
 *        properties:
 *          title:
 *            type: string
 *            description: Product name.
 *          shortDescription:
 *            type: string
 *            description: Short description.
 *          description:
 *            type: string
 *            description: Description.
 *          price:
 *            type: number
 *            description: Price.
 *          quantity:
 *            type: string
 *            description: quantity.
 */
 module.exports.addProduct = {
  body: {
    title: Joi.string().required(),
    shortDescription: Joi.string().optional().allow(''),
    description: Joi.string().optional().allow(''),
    price: Joi.number().required(),
    quantity: Joi.number().required()
  },
};

/**
   * @swagger
   *  components:
   *    schemas:
   *      ProductUpdate:
   *        type: object
   *        required:
   *          - title
   *          - shortDescription
   *          - description
   *          - price
   *          - quantity
   *        properties:
   *          title:
   *            type: string
   *            description: title for product.
   *          description:
   *            type: string
   *            description: description of the product.
   *          shortDescription:
   *            type: string
   *            description: shortDescription for the product
   *          price:
   *            type: number
   *            description: price for the product.
   *          quantity:
   *            type: number
   *            description: quantity of the product
*/
module.exports.update = {
  body: {
    title: Joi.string().required(),
    shortDescription: Joi.string().optional().allow(''),
    description: Joi.string().optional().allow(''),
    price: Joi.number().required(),
    quantity: Joi.number().required()
  }
};