/*******************************************************************************
 * Put Server and Plugins configs here
 * ENV: Development
 ******************************************************************************/
'use strict';
const projectName = 'TestApp',
  port = 8080,
  baseUrl = `http://localhost:${port}`,
  logoUrl = `http://localhost:${port}`,
  webAppBaseUrl = 'http://localhost:3000';

module.exports = {
  env: 'development',
  baseUrl,
  logoUrl,
  webAppBaseUrl,
  server: {
    host: '127.0.0.1',
    port: port,
  },
  product: {
    name: projectName,
  },
  saltRounds: 10,
  timeZone: 'Europe/Rome',
  key: {
    privateKey: 'BbZJjyoXAdr8BUZuiKKARWimKfrSmQ6fv8kZ7OFfc',
    tokenExpiry: 1 * 30 * 1000 * 60 * 24, //24 hour,
  },
  db: {
    url: 'mongodb://127.0.0.1:27017/testapp-db',
    options: {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    },
  },
  staticPaths: {
    uploadFile: process.cwd() + '/uploads/',
    imageMaxSize: 2 * 1024 * 1024 * 1024,
    file: baseUrl + 'uploads/',
  },
};
