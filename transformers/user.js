'use strict';

const userProfile = (user) => {
  let { _id, email, firstName, lastName } = user;
  return { _id, email, firstName, lastName };
};

const userloginDetails = (user) => {
  let { _id, email, firstName, lastName } = user;
  return { _id, email, firstName, lastName };
};

const userList = (users) => {
  return users.map(user => userProfile(user))
}

module.exports = {
  userProfile,
  userloginDetails,
  userList
};
