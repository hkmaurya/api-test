const productList = (products) => {
return products.map((product) => {
  let { _id, title, shortDescription, price, quantity, description, created_at } = product;
    return {_id, title, shortDescription, price, quantity, description, created_at } ;
  });
}

const productDetails = (product) => {
  let { _id, title, shortDescription, description, price, quantity, created_at } = product;
  return { _id, title, shortDescription, description, price, quantity, created_at };
};

module.exports = {
  productList,
  productDetails
}