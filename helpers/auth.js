/*******************************************************************************
 * Auth Helper
 ******************************************************************************/

'use strict';
const commonHelper = require('./common'),
  userHelper = require('./user');

module.exports = {
  userValidate: async (req, res, next) => {
    const token = req.headers['auth'] ? req.headers['auth'] : req.headers['authorization'];
    if (!token) return res.boom.badRequest(res.__('missingAuthorization'));
    const authToken = token.split(' ');
    if (authToken[0] !== 'Bearer') return res.boom.badRequest(res.__('invalidAuthorization'));
    const language = req.headers['accept-language'];
    if (!language) return res.boom.badRequest(res.__('missingLanguage'));
    if (!commonHelper.matchLanguage(language)) return res.boom.badRequest(res.__('invalidLanguage'));
    const decoded = await userHelper.jwtTokenVerify(authToken[1]);
    if (!decoded) return res.boom.forbidden(res.__('failedAuthenticateToken'));
    req.auth = {};
    req.auth.credentials = decoded;
    req.auth.token = token;
    next();
  },
  publicValidate: (req, res, next) => {
    const language = req.headers['accept-language'];
    if (!language) return res.boom.badRequest(res.__('missingLanguage'));
    if (!commonHelper.matchLanguage(language)) return res.boom.badRequest(res.__('invalidLanguage'));
    next();
  },
};
