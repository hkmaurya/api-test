const CommonHelper = require('./common');


const getProductId = data => {
  let id = CommonHelper.removeEmptySpace(data);
  return CommonHelper.isObjectIds(id) ? CommonHelper.stringToObjectIds(id) : '';
}

module.exports = {
  getProductId
}
